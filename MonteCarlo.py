from Puzzle import PuzzleState
import random


def monteCarloSearch(initialState: PuzzleState, numIterations: int = 100000):
    bestMoves = []
    bestScore = float('inf')

    for _ in range(numIterations):
        currentState = initialState
        moves = []
        score = 0

        while not currentState.isGoal():
            possibleMoves = currentState.getPossibleMoves()
            move = random.choice(possibleMoves)
            currentState = currentState.move(move)
            moves.append(move)
            score += 1

        if score < bestScore:
            bestMoves = moves
            bestScore = score

    return bestMoves