from collections import deque
from Puzzle import PuzzleState

def breadthFirstSearch(initialState: PuzzleState) -> list:
    visited = set()
    queue = deque([(initialState, [])])

    if sum(initialState.puzzle) != sum(initialState.goal):
        return None
    
    while queue:
        state, path = queue.popleft()

        if state.isGoal():
            return path

        if state in visited:
            continue

        visited.add(state)

        for move in state.getPossibleMoves():
            newState = state.move(move)
            if newState not in visited:
                queue.append((newState, path + [move]))

    return None