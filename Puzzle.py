from __future__ import annotations
from functools import total_ordering
from math import sqrt



def checkState(state: tuple) -> bool:
    size = int(len(state) ** 0.5)
    inversions = 0
    blankRow = 0

    for i in range(len(state)):
        if state[i] == 0:
            blankRow = size - i // size
            continue
        
        for j in range(i + 1, len(state)):
            if state[j] == 0:
                continue
            
            if state[i] > state[j]:
                inversions += 1

    if size % 2 == 1:
        return inversions % 2 == 0
    else:
        return (inversions + blankRow) % 2 == 1

@total_ordering
class PuzzleState:
    def __init__(self, puzzle: tuple, gScore: int = 0, hScore: int = 0) -> None:
        self.puzzle: tuple = puzzle
        self.size: int = int(sqrt(len(puzzle)))
        self.goal: tuple = tuple(range(1, self.size**2)) + (0,)  # target state
        self.gScore = gScore
        self.hScore = hScore
        
        
    def __eq__(self, other: tuple) -> bool:
        return self.puzzle == other.puzzle

    def __hash__(self) -> int:
        return hash(self.puzzle)

    def isGoal(self) -> bool:
        return self.puzzle == self.goal

    def getPossibleMoves(self) -> list:
        moves = []
        emptyIndex = self.puzzle.index(0)
        row = emptyIndex // self.size
        col = emptyIndex % self.size

        if row > 0:
            moves.append(-self.size)  # up
        if row < self.size - 1:
            moves.append(self.size)  # down
        if col > 0:
            moves.append(-1)  # left
        if col < self.size - 1:
            moves.append(1)  # right

        return moves

    def move(self, direction: int) -> PuzzleState:
        emptyIndex = self.puzzle.index(0)
        newIndex = emptyIndex + direction
        newPuzzle = list(self.puzzle)
        newPuzzle[emptyIndex], newPuzzle[newIndex] = newPuzzle[newIndex], newPuzzle[emptyIndex]
        
        return PuzzleState(tuple(newPuzzle))
    
    def calculateHeuristic(self):
        # Манхэттенское расстояние
        heuristic = 0
        for i in range(self.size):
            for j in range(self.size):
                tile = self.puzzle[i * self.size + j]
                
                if tile != 0:
                    goalRow = (tile - 1) // self.size
                    goalCol = (tile - 1) % self.size
                    heuristic += abs(i - goalRow) + abs(j - goalCol)
        
        return heuristic
    
    def __le__(self, other: PuzzleState) -> bool:
        return self.calculateHeuristic() <= other.calculateHeuristic()