from BFS import breadthFirstSearch
from AStar import aStarSearch
from MonteCarlo import monteCarloSearch
from Puzzle import PuzzleState, checkState


from time import perf_counter_ns



if __name__ == "__main__":



    n = int(input("Ведите размер поля n*n \n")) 
    initialPuzzle = tuple(map(int, input("Введите исходное состояние пятнашек через пробел \n").split()))
    #initialPuzzle = tuple(range(n**2))
    err = True

    if not all(isinstance(value, int) for value in initialPuzzle):
        err = False
   
    expected_values = set(range(n**2))
    unique_values = set(initialPuzzle)

    if unique_values != expected_values:
        missing_values = expected_values - unique_values
        err = False

    if err:
        print("1: Поиск в ширину")
        print("2: Монте-Карло")
        print("3: А*")

        key = int(input("Выберите алгоритм: "))

        if not checkState:
            key = False
        
        if key == 1:

            startTime = perf_counter_ns()
            initialState = PuzzleState(initialPuzzle)
            solution = breadthFirstSearch(initialState)
            result = []
            for value in solution:
                if value == -1:
                    result.append("left")
                elif value == 1:
                    result.append("right")
                elif value < -1:
                    result.append("up")
                elif value > 1:
                    result.append("down")

            result = tuple(result)

            if solution:
                print("Решение найдено:")
                print("Начальное состояние:", initialPuzzle)
                print("Шаги:", result)
                print("Количество шагов: ", len(solution))
            else:
                print("Начальное состояние:", initialPuzzle)
                print("Решение не найдено.")

            finalTime = perf_counter_ns()   
            
            print("Времени потребовалось:", (finalTime - startTime) / 10**9, "sec")

            
        elif key == 2:

            startTime = perf_counter_ns()
            initialState = PuzzleState(initialPuzzle)
            solution = monteCarloSearch(initialState, 1000000)
            result = []
            for value in solution:
                if value == -1:
                    result.append("left")
                elif value == 1:
                    result.append("right")
                elif value < -1:
                    result.append("up")
                elif value > 1:
                    result.append("down")

            result = tuple(result)
            if solution:
                print("Решение найдено:")
                print("Начальное состояние:", initialPuzzle)
                print("Шаги:", result)
                print("Количество шагов: ", len(solution))
            else:
                print("Начальное состояние:", initialPuzzle)
                print("Решение не найдено.")

            finalTime = perf_counter_ns()   
            
            print("Времени потребовалось:", (finalTime - startTime) / 10**9, "sec")

        elif key == 3:

            startTime = perf_counter_ns()
            initialState = PuzzleState(initialPuzzle)
            solution = aStarSearch(initialState)
            result = []
            for value in solution:
                if value == -1:
                    result.append("left")
                elif value == 1:
                    result.append("right")
                elif value < -1:
                    result.append("up")
                elif value > 1:
                    result.append("down")

            result = tuple(result)
            if solution:
                print("Решение найдено:")
                print("Начальное состояние:", initialPuzzle)
                print("Шаги:", result)
                print("Количество шагов: ", len(solution))
            else:
                print("Начальное состояние:", initialPuzzle)
                print("Решение не найдено.")

            finalTime = perf_counter_ns()   
            
            print("Времени потребовалось:", (finalTime - startTime) / 10**9, "sec")

        else:

            print("Невозможно собрать из текущего состояния")
    else:
        print("Данные введены некорректно. Отсутствуют числа:", missing_values)

   
    

