from collections import deque
import heapq
from Puzzle import PuzzleState


def aStarSearch(initialState: PuzzleState) -> list:
    openSet = []
    heapq.heappush(openSet, (initialState.gScore + initialState.hScore, initialState))
    gScores = {initialState: initialState.gScore}
    cameFrom = {}

    while openSet:
        _, currentState = heapq.heappop(openSet)

        if currentState.isGoal():
            path = reconstructPath(cameFrom, currentState)
            return path

        for move in currentState.getPossibleMoves():
            newState = currentState.move(move)
            newGScore = gScores[currentState] + 1

            if newState not in gScores or newGScore < gScores[newState]:
                gScores[newState] = newGScore
                newHScore = newState.calculateHeuristic()
                fScore = newGScore + newHScore
                heapq.heappush(openSet, (fScore, newState))
                cameFrom[newState] = currentState

    return None


def reconstructPath(cameFrom: list, currentState: PuzzleState) -> list:
    path = []
    while currentState in cameFrom:
        move = currentState.puzzle.index(0) - cameFrom[currentState].puzzle.index(0)
        path.append(move)
        currentState = cameFrom[currentState]
    path.reverse()
    return path
