from BFS import breadthFirstSearch
from AStar import aStarSearch
from MonteCarlo import monteCarloSearch
from Puzzle import PuzzleState, checkState

from time import perf_counter_ns
from prettytable import PrettyTable
import random


if __name__ == "__main__":
    results = {"breadthFirstSearch": [],
               "aStarSearch": [],
               "monteCarloSearch": []
               }
    
    samples = 100
    
    n = 2
    pool = range(n*n)
    for i in range(samples):
        initialPuzzle = tuple(random.sample(pool, n*n))
            
        if not checkState(initialPuzzle):
            continue
        
        initialState = PuzzleState(initialPuzzle)
        
        startTime = perf_counter_ns()
        solution = breadthFirstSearch(initialState)
        finalTime = perf_counter_ns()
        
        results["breadthFirstSearch"].append((len(solution), (finalTime-startTime) / 10**9))
        
        startTime = perf_counter_ns()
        solution = aStarSearch(initialState)
        finalTime = perf_counter_ns()
        
        
        results["aStarSearch"].append((len(solution), (finalTime-startTime) / 10**9))
        
        startTime = perf_counter_ns()
        solution = monteCarloSearch(initialState)
        finalTime = perf_counter_ns()
        
        results["monteCarloSearch"].append((len(solution), (finalTime-startTime) / 10**9))
        
    table = PrettyTable(results.keys())
    row = []
    for key in results.keys():
        steps, times = zip(*results[key])
        row.append((sum(steps)/len(steps), sum(times)/len(times)))
    
    table.add_row(row)
    
    print(table)
    
    